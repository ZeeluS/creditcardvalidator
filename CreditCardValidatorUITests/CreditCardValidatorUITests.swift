//
//  CreditCardValidatorUITests.swift
//  CreditCardValidatorUITests
//
//  Created by Gilbert Gwizdała on 06.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import XCTest

class CreditCardValidatorUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()

    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testWrongNumberValidiation() {
        
        let app = XCUIApplication()
        let textField = app.textFields["cardNumberTextField"]
        textField.tap()
        textField.typeText("1111111111111111")
        app.toolbars.buttons["Done"].tap()
        app.buttons["Validate"].tap()
        
        _ = app.waitForExistence(timeout: 5.0)
        
        let label = app.staticTexts.element(matching: .any, identifier: "progressLabel").label
        XCTAssertTrue(label != "Card number is validate !")
    }
    
    func testCorrectNumberValidiation() {
        
        let app = XCUIApplication()
        let textField = app.textFields["cardNumberTextField"]
        textField.tap()
        textField.typeText("4916958306015441")
        app.toolbars.buttons["Done"].tap()
        app.buttons["Validate"].tap()
        
        _ = app.waitForExistence(timeout: 5.0)
        
        let label = app.staticTexts.element(matching: .any, identifier: "progressLabel").label
        XCTAssertTrue(label == "Card number is validate !")
    }
    
    func testCorrectGeneratedNumberValidiation() {
        
        let app = XCUIApplication()
        app.buttons["Generate"].tap()
        app.buttons["Validate"].tap()
        
        _ = app.waitForExistence(timeout: 5.0)
        
        let label = app.staticTexts.element(matching: .any, identifier: "progressLabel").label
        XCTAssertTrue(label == "Card number is validate !")
    }
    
    
}
