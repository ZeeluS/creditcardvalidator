//
//  CreditCardValidatorTests.swift
//  CreditCardValidatorTests
//
//  Created by Gilbert Gwizdała on 06.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import XCTest
@testable import CreditCardValidator

class CardNumberGeneratorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testLunaAlgoritmReturn() {
        let generator = CardNumberGenerator()
        // 4917 7427 9422 5277
        let numbers = [4, 9, 1, 7, 7, 4, 2, 7, 9, 4, 2, 2, 5 ,2, 7] // last digit should by 7
        
        let lastDigit = generator.getLunaNumber(numbers)
        
        XCTAssertTrue(lastDigit == 7)
    }
    
    func testStringValidatorTrue() {
        let generator = CardNumberGenerator()
        
        let string = "4917 7427 9422 5277"
        
        XCTAssertTrue(generator.validNumber(string: string))
    }
    
    func testStringValidatorFalse() {
        let generator = CardNumberGenerator()
        
        let string = "4917 7427 9422 5270"
        
        XCTAssertFalse(generator.validNumber(string: string))
    }
    
    func testGenerateNumber() {
        let generator = CardNumberGenerator()
        
        guard let numberString = generator.generateRandomNumber() else { XCTFail("Nil value"); return }
        
        XCTAssertTrue(generator.validNumber(string: numberString))
    }

    
}
