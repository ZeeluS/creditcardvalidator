//
//  AppDelegate.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 04.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.window = UIWindow()
        self.window?.rootViewController = CodeInputViewControler()
        self.window?.makeKeyAndVisible()
        
        return true
    }

}

