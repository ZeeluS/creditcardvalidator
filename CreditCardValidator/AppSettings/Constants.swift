//
//  Constants.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 05.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import Foundation

struct Constants {
    static let binCodesApiURL: URL = URL(string: "https://api.bincodes.com/cc/")!
    static let apiKey = "5232a9bca11e25c0f8eb4313ff2644be"
}
