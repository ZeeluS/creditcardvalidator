//
//  ValidationModel.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 05.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import Foundation

struct ValidationModel: Codable {
    
    var bin: String?
    var bank: String?
    var card: String?
    var type: String?
    var level: String?
    var country: String?
    var countrycode: String?
    var website: String?
    var phone: String?
    var valid: String?
    
    var error: String?
    var message: String?
    

}
