//
//  CardNumberGenerator.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 06.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import Foundation

class CardNumberGenerator {
    
    private let initNumbers:[[Int]] = [
        [5, 1], //MasterCard
        [5, 2], //MasterCard
        [5, 3], //MasterCard
        [5, 4], //MasterCard
        [5, 5], //MasterCard
        [4], //Visa
        [4, 0, 2, 6], //Visa Electron
        [4, 1, 7, 5, 0, 0], //Visa Electron
        [4, 5, 0, 8], //Visa Electron
        [4, 8, 4, 4], //Visa Electron
        [4, 9, 1, 3], //Visa Electron
        [4, 9, 1, 7] //Visa Electron
    ]
    
    func generateRandomNumber() -> String? {
        var numbers: [Int] = []
        
        let initNumber = self.initNumbers[Int(arc4random()) % initNumbers.count]
        
        numbers.append(contentsOf: initNumber)
        
        for _ in numbers.count...14 {
            let num = Int(arc4random()) % 10
            numbers.append(num)
        }
        
        guard let lunaNumber = self.getLunaNumber(numbers) else { return nil}
        numbers.append(lunaNumber)
        return self.string(from: numbers)
    }
    
    func getLunaNumber(_ numbers: [Int]) -> Int? {
        guard numbers.count == 16 || numbers.count == 15 else { return nil }
        var sum: Int = 0
        
        for (index, number) in numbers.enumerated() {
            if index > 14 { break }
            var numberToSum: Int = ((index % 2) == 0 ? number << 1 : number )
            
            if numberToSum > 9 {
                let decimal: Int = numberToSum / 10
                let uniti: Int = numberToSum - (decimal * 10)
                numberToSum = decimal + uniti
            }
            
            sum += numberToSum
        }
        
        let moduloDecimal = sum % 10
        
        if moduloDecimal != 0 {
            return 10 - moduloDecimal
        } else {
            return 0
        }
        
    }
    
    func validNumber(string: String) -> Bool {
        let stringToAnlize = string.clearStringNumber()
        let numbers = stringToAnlize
            .map{ Int(String($0)) }
            .filter{ $0 != nil }
            .map{ $0! }
        
        if stringToAnlize.count != numbers.count {
            return false
        }
        
        return validLuna(numbers)
    }
    
    private func validLuna(_ numbers: [Int]) -> Bool {
        return getLunaNumber(numbers) == numbers.last
    }
    
    private func string(from numbers: [Int]) -> String {
        return numbers.reduce("", { (string, number) -> String in
            return "\(string)\(number)"
        })
    }
    
}
