//
//  CodeInputViewControler.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 04.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CodeInputViewControler: UIViewController {
    
    let viewModel = CodeInputViewModel()
    
    fileprivate let disposeBag = DisposeBag()
    
    private let inputTextsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    fileprivate let cardNumberTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "1234 5678 9012 3456"
        textField.keyboardType = .numberPad
        textField.accessibilityIdentifier = "cardNumberTextField"
        textField.tag = 0
        return textField
    }()
    
    private let monthAndYearTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "MM/YY"
        textField.keyboardType = .numberPad
        textField.accessibilityIdentifier = "monthAndYearTextField"
        textField.tag = 1
        return textField
    }()
    
    private let CVCTextField: UITextField = {
        let textField = UITextField()
        textField.placeholder = "CVC"
        textField.keyboardType = .numberPad
        textField.accessibilityIdentifier = "CVCTextField"
        textField.tag = 2
        return textField
    }()
    
    private let buttonsStackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.distribution = .fillEqually
        return stackView
    }()
    
    private let validateButton: UIButton = {
        let button = UIButton()
        button.setTitle("Validate", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.addTarget(self, action: #selector(validateButtonTap(_:)), for: .touchUpInside)
        return button
    }()
    
    private let generateButton: UIButton = {
        let button = UIButton()
        button.setTitle("Generate", for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.textColor = .black
        button.addTarget(self, action: #selector(generateButtonTap(_:)), for: .touchUpInside)
        return button
    }()
    
    private let progressLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        label.accessibilityIdentifier = "progressLabel"
        return label
    }()
    
    private let indicatorView: UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView()
        indicator.isHidden = true
        indicator.color = .black
        return indicator
    }()
    
    override func loadView() {
        self.view = UIView()
        self.view.backgroundColor = .white
        self.setupSubViews()
        self.setupConstraints()
        self.addDoneButtonOnKeyboards()
    }
    
    override func viewDidLoad() {
        self.setupDelegates()
        self.setupObservers()
    }
    
    private func setupDelegates() {
        self.cardNumberTextField.delegate = self
        self.monthAndYearTextField.delegate = self
        self.CVCTextField.delegate = self
    }
    
    private func setupSubViews() {
        self.view.addSubview(self.inputTextsStackView)
        self.inputTextsStackView.addArrangedSubview(self.cardNumberTextField)
        self.inputTextsStackView.addArrangedSubview(self.monthAndYearTextField)
        self.inputTextsStackView.addArrangedSubview(self.CVCTextField)
        self.view.addSubview(self.buttonsStackView)
        self.buttonsStackView.addArrangedSubview(self.validateButton)
        self.buttonsStackView.addArrangedSubview(self.generateButton)
        self.view.addSubview(self.progressLabel)
        self.view.addSubview(self.indicatorView)
    }
    
    private func setupConstraints() {
        
        self.inputTextsStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate( [
            self.inputTextsStackView.topAnchor.constraint(equalTo: self.topLayoutGuide.bottomAnchor),
            self.inputTextsStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8.0),
            self.inputTextsStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8.0),
            self.inputTextsStackView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.5)
        ])
        
        self.buttonsStackView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate( [
            self.buttonsStackView.bottomAnchor.constraint(equalTo: self.bottomLayoutGuide.topAnchor),
            self.buttonsStackView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.buttonsStackView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.buttonsStackView.heightAnchor.constraint(equalTo: self.view.heightAnchor, multiplier: 0.1)
        ])
        
        self.progressLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate( [
            self.progressLabel.bottomAnchor.constraint(equalTo: self.buttonsStackView.topAnchor, constant: -8.0),
            self.progressLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8.0),
            self.progressLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8.0),
            self.progressLabel.heightAnchor.constraint(equalToConstant: 40.0)
        ])
        
        self.indicatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate( [
            self.indicatorView.topAnchor.constraint(equalTo: self.inputTextsStackView.bottomAnchor),
            self.indicatorView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            self.indicatorView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            self.indicatorView.bottomAnchor.constraint(equalTo: self.progressLabel.topAnchor)
        ])
        
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        switch self.traitCollection.horizontalSizeClass {
        case .compact:
            self.inputTextsStackView.axis = .vertical
        case .regular:
            self.inputTextsStackView.axis = .horizontal
        default:
            break
        }
        
    }
    
    fileprivate func closeKeyboards() {
        self.cardNumberTextField.resignFirstResponder()
        self.monthAndYearTextField.resignFirstResponder()
        self.CVCTextField.resignFirstResponder()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.closeKeyboards()
    }
    
    @objc private func validateButtonTap(_ sender: UIButton) {
        guard let numberString = self.cardNumberTextField.text, numberString.isCardNumberString() else { return }
        self.viewModel.validate(numberString)
    }
    
    @objc private func generateButtonTap(_ sender: UIButton) {
        self.viewModel.generateCardNumber()
    }
    
    private func addDoneButtonOnKeyboards() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.cardNumberTextField.inputAccessoryView = doneToolbar
        self.monthAndYearTextField.inputAccessoryView = doneToolbar
        self.CVCTextField.inputAccessoryView = doneToolbar
    }
    
    @objc private func doneButtonAction() {
        self.closeKeyboards()
    }
    
}

// MARK: - UITextFieldDelegate
extension CodeInputViewControler: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        switch textField.tag {
        case 0:
            guard range.length == 0 else { return true }
            if textField.text?.count ?? 19 >= 19 {
                return false
            }
            
            switch range.location {
                case 4:
                    textField.text?.append(" ")
                case 9:
                    textField.text?.append(" ")
                case 14:
                    textField.text?.append(" ")
            default:
                break
            }
            
        case 1:
            
            guard range.length == 0 else { return true }
            
            if textField.text?.count ?? 5 >= 5 {
                return false
            }
            
            if range.location == 2 {
                textField.text?.append("/")
            }
            
        case 2:
            guard range.length == 0 else { return true }
            
            if textField.text?.count ?? 3 >= 3 {
                return false
            }
            
        default:
            break
        }
        
        return true
    }
    
    fileprivate func validateButton(isEnable: Bool) {
        self.validateButton.isEnabled = isEnable
        self.validateButton.alpha = isEnable ? 1.0 : 0.3
    }
    
}

// MARK: - Rx observers
extension CodeInputViewControler {
    
    fileprivate func setupObservers() {
        self.setupValidiationButtonObserver()
        self.setupProgresingViewObserver()
        self.setupProgressLabelObserver()
        self.setupGenerateStringObserver()
    }
    
    private func setupValidiationButtonObserver() {
        self.cardNumberTextField
            .rx
            .text
            .map{ $0?.isCardNumberString() }
            .map{ $0 ?? false }
            .asObservable().bind {[weak self] (isValidate) in
                self?.validateButton(isEnable: isValidate)
            }
            .disposed(by: self.disposeBag)
    }
    
    private func setupProgresingViewObserver() {
        self.viewModel
            .progressing
            .asDriver()
            .asObservable()
            .bind {[weak self] (isProgressing) in
                
                if isProgressing {
                    self?.indicatorView.startAnimating()
                } else {
                    self?.indicatorView.stopAnimating()
                }
                
                self?.indicatorView.isHidden = !isProgressing
                
                if let isNumberString = self?.cardNumberTextField.text?.isCardNumberString() {
                    self?.validateButton(isEnable: isNumberString)
                }
            }
            .disposed(by: self.disposeBag)
    }
    
    private func setupProgressLabelObserver() {
        self.viewModel
            .message
            .asDriver()
            .asObservable()
            .bind(to: self.progressLabel.rx.text)
            .disposed(by: self.disposeBag)
    }
    
    private func setupGenerateStringObserver() {
        self.viewModel
            .generateNumber
            .asDriver()
            .asObservable()
            .bind(to: self.cardNumberTextField.rx.text)
            .disposed(by: self.disposeBag)
    }
}
