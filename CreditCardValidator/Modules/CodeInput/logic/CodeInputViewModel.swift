//
//  CodeInputViewModel.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 05.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

class CodeInputViewModel {
    
    public let progressing = BehaviorRelay<Bool>(value: false)
    public let message = BehaviorRelay<String?>(value: nil)
    public let generateNumber = BehaviorRelay<String?>(value: nil)
    
    private let generator = CardNumberGenerator()
    
    private let disposeBag = DisposeBag()
    
    public func validate(_ string: String) {
        
        self.progressing.accept(true)
        self.message.accept("Progressing ...")
        
        NetworkMenager.validate(cardNumberString: string)
            .asObservable()
            .subscribe(onNext: {[weak self] (newModel) in
                    self?.analizeNew(model: newModel)
                }, onError: {[weak self] (error) in
                    self?.analizeError(error: error)
                },
               onCompleted: nil,
               onDisposed: nil
            ).disposed(by: self.disposeBag)
    }
    
    public func generateCardNumber() {
        self.message.accept(nil)
        DispatchQueue.global(qos: .background).async {
            guard let numberString = self.generator.generateRandomNumber() else { return }
            let stringToShow = numberString.presettingString()
            self.generateNumber.accept(stringToShow)
        }
    }
    
    
    private func analizeNew(model: ValidationModel) {
        self.progressing.accept(false)
        self.message.accept(nil)
        
        if let error = model.error {
            self.message.accept("\(error): \(model.message ?? "")")
            return
        }
        
        self.message.accept("Card number is \(model.valid == "true" ? "validate" : "novalidate") !")
        
    }
    
    private func analizeError(error: Error) {
        self.progressing.accept(false)
        self.message.accept(error.localizedDescription)
    }
    
}
