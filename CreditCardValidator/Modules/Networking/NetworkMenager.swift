//
//  NetworkMenager.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 05.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import Alamofire
import RxSwift

struct NetworkMenager {
    
    private init() {}
    
    static func validate(cardNumberString: String) -> Observable<ValidationModel> {
        return Alamofire.request(Constants.binCodesApiURL,
                                 method: .get,
                                 parameters: ["format": "json", "api_key": Constants.apiKey, "cc": cardNumberString],
                                 encoding: URLEncoding.default,
                                 headers: nil)
                                .rx
                                .responseObject()
    }
    
}
