//
//  Alamofire+RxSwift.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 05.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//
import Alamofire
import RxSwift

extension Request: ReactiveCompatible {}

extension Reactive where Base: DataRequest {
    
    func responseObject<T: Decodable>() -> Observable<T> {
        return Observable.create { observer in
            let request = self.base.responseData { response in
                switch response.result {
                case .success(let value):
                    do {
                        let object = try JSONDecoder().decode(T.self, from: value)
                        observer.onNext(object)
                    }
                    catch(let error) {
                        observer.onError(error)
                    }
                    observer.onCompleted()
                    
                case .failure(let error):
                    observer.onError(error)
                }
            }
            
            return Disposables.create(with: request.cancel)
        }
    }
}
