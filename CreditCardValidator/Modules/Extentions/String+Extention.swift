//
//  String+Extention.swift
//  CreditCardValidator
//
//  Created by Gilbert Gwizdała on 05.04.2018.
//  Copyright © 2018 Gilbert Gwizdała. All rights reserved.
//

import Foundation

extension String {
    
    func clearStringNumber() -> String {
        return self.replacingOccurrences( of:"[^0-9]", with: "", options: .regularExpression)
    }
    
    func isCardNumberString() -> Bool {
        return self.clearStringNumber().count == 16
    }
    
    func presettingString() -> String {
        var string = ""
        for (index, char) in self.enumerated() {
            if index != 0 && (index % 4) == 0 {
                string.append(" ")
            }
            string.append(char)
        }
        
        return string
    }
}
